import sys
import torch

import utils as u
from Models import ConvNet
from api import fit, predict

#####################################################################################################

def app():

    args_0 = "--colab"
    args_1 = "--model-name"
    args_2 = "--pretrained"
    args_3 = "--image-size"
    args_4 = "--bs"
    args_5 = "--lr"
    args_6 = "--wd"
    args_7 = "--scheduler"
    args_8 = "--epochs"
    args_9 = "--early"
    args_10 = "--test"
    args_11 = "--name"
    args_12 = "--debug"

    in_colab = None
    model_name = None
    pretrained = False
    image_size = 224
    batch_size, lr, wd = 64, 1e-3, 0
    do_scheduler, scheduler = None, None
    epochs, early_stopping = 10, 5
    train_mode = True
    name = None
    do_debug = None

    if args_0 in sys.argv: in_colab = True
    if args_1 in sys.argv: model_name = sys.argv[sys.argv.index(args_1) + 1]
    if args_2 in sys.argv: pretrained = True
    if args_3 in sys.argv: image_size = int(sys.argv[sys.argv.index(args_3) + 1])
    if args_4 in sys.argv: batch_size = int(sys.argv[sys.argv.index(args_4) + 1])
    if args_5 in sys.argv: lr = float(sys.argv[sys.argv.index(args_5) + 1])
    if args_6 in sys.argv: wd = float(sys.argv[sys.argv.index(args_6) + 1])
    if args_7 in sys.argv:
        do_scheduler = True
        patience = int(sys.argv[sys.argv.index(args_7) + 1])
        eps = float(sys.argv[sys.argv.index(args_7) + 2])
    if args_8 in sys.argv: epochs = int(sys.argv[sys.argv.index(args_8) + 1])
    if args_9 in sys.argv: early_stopping = int(sys.argv[sys.argv.index(args_9) + 1])
    if args_10 in sys.argv: train_mode = False
    if args_11 in sys.argv: name = sys.argv[sys.argv.index(args_11) + 1]
    if args_12 in sys.argv: do_debug = True

    assert(model_name  is not None)
    torch.manual_seed(u.SEED)
    model = ConvNet(model_name=model_name, pretrained=pretrained)

    if train_mode:
        dataloaders = u.build_dataloaders(batch_size, image_size, pretrained, in_colab)

        if do_debug:
            import random as r
            index = r.randint(0, batch_size-1)

            u.breaker()
            u.myprint("--- DEBUGGING MODE ---", "red")

            xx, _ = next(iter(dataloaders["train"]))
            u.show(xx[index].detach().cpu().numpy().transpose(1, 2, 0))
            xx, _ = next(iter(dataloaders["valid"]))
            u.show(xx[index].detach().cpu().numpy().transpose(1, 2, 0))
        
        optimizer = model.get_optimizer(lr=lr, wd=wd)
        if do_scheduler:
            scheduler = model.get_plateau_scheduler(patience=patience, eps=eps)
        
        L, A, _, _ = fit(model=model, optimizer=optimizer, scheduler=scheduler, epochs=epochs,
                        early_stopping_patience=early_stopping, dataloaders=dataloaders, verbose=True)
        
        u.save_graphs(L, A)
    
    else:
        if in_colab:
            image = u.get_image(u.TEST_PATH_2, name)
        else:
            image = u.get_image(u.TEST_PATH_1, name)

        if pretrained:
            label = predict(model, image, image_size, u.TRANSFORM_1)
        else:
            label = predict(model, image, image_size, u.TRANSFORM_2)
        
        u.breaker()
        u.myprint(label, "green")
        u.breaker()

    u.myprint("Execution Completed. Terminating ...", "yellow")
    u.breaker()

#####################################################################################################
