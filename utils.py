import os
import re
import cv2
import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from termcolor import colored
from torchvision import transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader as DL
from sklearn.model_selection import train_test_split

import warnings
warnings.filterwarnings("ignore")
os.system("color")

#####################################################################################################

class DS(Dataset):
    def __init__(self, filenames=None, labels=None, image_size=None, in_colab=None, transform=None, mode="train"):
        self.mode = mode
        self.transform = transform

        assert(re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE) or re.match(r"test", self.mode, re.IGNORECASE))
        
        self.image_size = image_size
        self.in_colab = in_colab
        self.filenames = filenames
        if re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE):
            self.labels = labels.reshape(-1, 1)

    def __len__(self):
        return self.filenames.shape[0]
    
    def __getitem__(self, idx):
        if re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE):
            if self.in_colab:
                image = get_image(TRAIN_PATH_2, self.filenames[idx])
            else:
                image = get_image(TRAIN_PATH_1, self.filenames[idx])
            return self.transform(resized(image, self.image_size)), torch.LongTensor(self.labels[idx])
        else:
            if self.in_colab:
                image = get_image(TRAIN_PATH_2, self.filenames[idx])
            else:
                image = get_image(TRAIN_PATH_1, self.filenames[idx])
            return self.transform(resized(image, self.image_size))

#####################################################################################################

def myprint(text: str, color: str):
    print(colored(text=text, color=color))


def breaker(num=50, char="*"):
    myprint("\n" + num*char + "\n", "magenta")

#####################################################################################################

def get_image(path: str, name: str) -> np.ndarray:
    return cv2.cvtColor(src=cv2.imread(os.path.join(path, name)), code=cv2.COLOR_BGR2RGB)


def resized(image: np.ndarray, size: int) -> np.ndarray:
    return cv2.resize(src=image, dsize=(size, size), interpolation=cv2.INTER_AREA)


def show(image: np.ndarray, title=None) -> None:
    plt.figure()
    plt.imshow(image)
    plt.axis("off")
    if title:
        plt.title(title)
    plt.show()


def save_graphs(L: list, A: list) -> None:
    TL, VL, TA, VA = [], [], [], []
    for i in range(len(L)):
        TL.append(L[i]["train"])
        VL.append(L[i]["valid"])
        TA.append(A[i]["train"])
        VA.append(A[i]["valid"])
    x_Axis = np.arange(1, len(TL) + 1)
    plt.figure("Plots")
    plt.subplot(1, 2, 1)
    plt.plot(x_Axis, TL, "r", label="Train")
    plt.plot(x_Axis, VL, "b", label="Valid")
    plt.legend()
    plt.grid()
    plt.title("Loss Graph")
    plt.subplot(1, 2, 2)
    plt.plot(x_Axis, TA, "r", label="Train")
    plt.plot(x_Axis, VA, "b", label="Valid")
    plt.legend()
    plt.grid()
    plt.title("Accuracy Graph")
    plt.savefig("./Graphs.jpg")
    plt.close("Plots")

#####################################################################################################

def build_dataloaders(batch_size: int, image_size: int, pretrained=None, in_colab=None):
    breaker()
    myprint("Reading CSV File ...", "yellow")

    if in_colab:
        train_info = pd.read_csv(TRAIN_INFO_2, engine="python")
    else:
        train_info = pd.read_csv(TRAIN_INFO_1, engine="python")

    train_info["Class"] = train_info["Class"].map({"YOUNG" : 0, "MIDDLE" : 1, "OLD" : 2})
    
    breaker()
    myprint("Splitting into train and validation sets ...", "yellow")
    filenames = train_info["ID"].copy().values
    labels = train_info["Class"].copy().values
    tr_filenames, va_filenames, tr_labels, va_labels = train_test_split(filenames, labels, test_size=0.2, shuffle=True, random_state=SEED, stratify=labels)

    del train_info, filenames, labels

    breaker()
    myprint("Building Dataloaders ...", "yellow")
    if pretrained:
        tr_data_setup = DS(filenames=tr_filenames, labels=tr_labels, image_size=image_size, in_colab=in_colab, transform=TRANSFORM_1, mode="train")
        va_data_setup = DS(filenames=va_filenames, labels=va_labels, image_size=image_size, in_colab=in_colab, transform=TRANSFORM_1, mode="valid")
    else:
        tr_data_setup = DS(filenames=tr_filenames, labels=tr_labels, image_size=image_size, in_colab=in_colab, transform=TRANSFORM_2, mode="train")
        va_data_setup = DS(filenames=va_filenames, labels=va_labels, image_size=image_size, in_colab=in_colab, transform=TRANSFORM_2, mode="valid")
    
    dataloaders = {
        "train" : DL(tr_data_setup, batch_size=batch_size, shuffle=True, generator=torch.manual_seed(SEED)),
        "valid" : DL(va_data_setup, batch_size=batch_size, shuffle=False)
    }

    return dataloaders

#####################################################################################################

TRAIN_PATH_1 = "./data/Train"
TRAIN_INFO_1 = "./data/train.csv"

TEST_PATH_1 = "./Test"

TRAIN_PATH_2 = "/.data/Train"
TRAIN_INFO_2 = "/.data/train.csv"

TEST_PATH_2 = "/content"

CHECKPOINT_PATH = "./Checkpoints"
if not os.path.exists(CHECKPOINT_PATH):
    os.makedirs(CHECKPOINT_PATH)

TRANSFORM_1 = transforms.Compose([transforms.ToTensor(), transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
TRANSFORM_2 = transforms.Compose([transforms.ToTensor(), ])
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
SEED = 0
LABELS = ["YOUNG", "MIDDLE", "OLD"]

#####################################################################################################