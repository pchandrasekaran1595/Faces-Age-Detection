Predicting the age based on facial features. Uses the dataset at https://www.kaggle.com/arashnic/faces-age-detection-dataset


---

&nbsp;

### **CLI Arguements**

<pre>
0. --colab       - Flag that allows for testing within th colab environment.
1. --model-name  - Name of the model (Supports 'resnet', 'vgg' & 'mobilenet')
2. --pretrained  - Flag that controls whether to use a pretrained model (Default: False)
3. --image-size  - Size of image passed to the network; do not modify if pretrained is in use (Default: 224)
4. --bs          - Batch Size (Default: 64)
5. --lr          - Learing Rate (Default: 1e-3)
6. --wd          - Weight Decay (Default: 0)
7. --scheduler   - Needs two arguments; patience and eps
8. --epochs      - Number of training epochs (Default: 10)
9. --early       - Early stopping patience (Default: 5)
10. --test       - Flag that controls entry into test mode (Default: False)
11. --name       - Name of the image file to be tested (Default: None)
</pre>

&nbsp;

---